#### Hi there 👋 I'm Estee and I am a -

<h3 style="font-size: 2rem;">FullStack Web Developer</h3>
<hr>

<h3> View My Projects On My Portfolio Website ↓</h3>
<hr>

<!--
|   |   |   |
| - | - | - |
| [<img src="https://github.com/monacodelisa/portfolio-2023-angular-dev/blob/main/portfolio-2023-angular/src/assets/images/monacodelisa-site.jpg?raw=true"  width="350">](https://monacodelisa.com/) |  [<img src="https://github.com/monacodelisa/portfolio-2023-angular-dev/blob/main/portfolio-2023-angular/src/assets/images/monacodelisa-site.jpg?raw=true"  width="350">](https://monacodelisa.com/) |  [<img src="https://github.com/monacodelisa/portfolio-2023-angular-dev/blob/main/portfolio-2023-angular/src/assets/images/monacodelisa-site.jpg?raw=true"  width="350">](https://monacodelisa.com/) |
| <h3>My Portfolio Site</h3><small>The difference between my portfolio and GitHub is not only my beautiful design. On my portfolio website you van view both my Public & Private Repos</small> | - | - |
-->

<p>My portfolio, unlike GitHub, showcases not just my development skills <br> but also allows you to explore projects from both my Public and Private Repos.</p>
    <a href="https://monacodelisa.com/" target="_blank"><img width="500px" src="https://monacodelisa.com/assets/images/projects/monacodelisa-site.jpg"></a>
<hr>

<p>My GithubUnwrapped for 2023</p>

[![Alt text](https://github.com/monacodelisa/icons-and-graphics/blob/main/github-unwrapped-sm-2023.jpg?raw=true)](https://youtube.com/shorts/owg_Lwzv3oo?si=gSV7S3MV1gVSBbjF)


| <p>Hire me on</p><a href='https://www.upwork.com/freelancers/~01d02763fe3eb55269' target='_blank'><img height='36' style='border:0px;height:36px;' src='https://github.com/monacodelisa/icons-and-graphics/blob/main/upwork.png?raw=true' border='0' alt='Hire me on upwork' /></a> | <p>Or just</p><a href='https://ko-fi.com/monacodelisa' target='_blank'><img height='36' style='border:0px;height:36px;' src='https://cdn.ko-fi.com/cdn/kofi2.png?v=3' border='0' alt='Buy Me a Coffee at ko-fi.com' /></a> | 
| - | -

<h3>Connect with me on Social Media ↓</h3>
<a href="https://www.linkedin.com/in/monacodelisa/" target="_blank"><img src="https://github.com/monacodelisa/icons-and-graphics/blob/main/icomoon/PNG/linkedin.png?raw=true"></a>
<a href="https://codepen.io/monacodelisa" target="_blank"><img src="https://github.com/monacodelisa/icons-and-graphics/blob/main/icomoon/PNG/codepen.png?raw=true"></a>
<a href="https://dev.to/monacodelisa" target="_blank"><img src="https://github.com/monacodelisa/icons-and-graphics/blob/main/icomoon/PNG/dev-dot-to.png?raw=true"></a>
<a href="https://twitter.com/monacodelisa" target="_blank"><img src="https://github.com/monacodelisa/icons-and-graphics/blob/main/icomoon/PNG/twitter.png?raw=true"></a>
<a href="https://www.youtube.com/c/monacodelisa" target="_blank"><img src="https://github.com/monacodelisa/icons-and-graphics/blob/main/icomoon/PNG/youtube.png?raw=true"></a>
<a href="https://www.instagram.com/monacodelisa/" target="_blank"><img src="https://github.com/monacodelisa/icons-and-graphics/blob/main/icomoon/PNG/instagram.png?raw=true"></a>
<a href="https://www.tiktok.com/@monacodelisa" target="_blank"><img src="https://github.com/monacodelisa/icons-and-graphics/blob/main/icomoon/PNG/tiktok.png?raw=true"></a>
<a href="https://www.twitch.tv/monacodelisa" target="_blanc"><img src="https://github.com/monacodelisa/icons-and-graphics/blob/main/icomoon/PNG/twitch.png?raw=true"></a>
<br>

<!--
- 🔭 I’m currently working on ...
- 👯 I’m looking to collaborate on ...
- 🤔 I’m looking for help with ...
- 💬 Ask me about ...
- 📫 How to reach me: ...
- 😄 Pronouns: ...
- ⚡ Fun fact: ...
-->
